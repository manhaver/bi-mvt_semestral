using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Eraser : MonoBehaviour
{
    private Color color;
    private Whiteboard whiteboard;
    private Color[] pixels;

    void Start()
    {
        whiteboard = GameObject.FindGameObjectWithTag("Whiteboard").GetComponent<Whiteboard>();
        color = whiteboard.GetComponent<Renderer>().material.color;
    }

    public void ClearWhiteboard ()
    {
        pixels = Enumerable.Repeat(color, whiteboard.texture.width * whiteboard.texture.height).ToArray();
        whiteboard.texture.SetPixels(pixels);
        whiteboard.texture.Apply();
    }
}
