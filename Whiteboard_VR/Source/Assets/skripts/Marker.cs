using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Marker : MonoBehaviour
{   
    public Transform markerTip;
    public int size = 5;
    private Renderer componentRenderer;
    private Color[] colors;
    private float tipHeight;
    private RaycastHit touch;
    private Whiteboard whiteboard;
    private Vector2 touchPos;
    private bool touchedLastFrame;
    private Vector2 lastTouchPos;
    private Quaternion lastTouchRot;

    void Start()
    {
        componentRenderer = markerTip.GetComponent<Renderer>();
        colors = Enumerable.Repeat(componentRenderer.material.color, size * size).ToArray();
        tipHeight = markerTip.localScale.y;
        
    }

    void Update()
    {
        //if we touch something with the marker
        if(Physics.Raycast(markerTip.position, transform.up, out touch, tipHeight))
        {
            //if we touch whiteboard
            if(touch.transform.CompareTag("Whiteboard"))
            {   
                //seting whiteboard only the first time called Draw()
                if(whiteboard == null)
                {
                    whiteboard = touch.transform.GetComponent<Whiteboard>();
                    
                }

                //position of the touch on the whiteboard
                touchPos = new Vector2(touch.textureCoord.x, touch.textureCoord.y);

                //get pixel number of the touchPos
                var x = (int)(touchPos.x * whiteboard.textureSize.x - (size/2));
                var y = (int)(touchPos.y * whiteboard.textureSize.y - (size/2));

                //if marker gets out of bounds of the whiteboard
                if(x < 0 || x > whiteboard.textureSize.x || y < 0 || y > whiteboard.textureSize.y) return;

                if(touchedLastFrame)
                {
                    //draw in pixels we touched
                    whiteboard.texture.SetPixels(x, y, size, size, colors);

                    //draw between the touches to get full line
                    for(float f = 0.01f; f < 1.00f; f += 0.02f)
                    {
                        var lerpX = (int)Mathf.Lerp(lastTouchPos.x, x, f);
                        var lerpY = (int)Mathf.Lerp(lastTouchPos.y, y, f);

                        whiteboard.texture.SetPixels(lerpX, lerpY, size, size, colors);
                    }

                    //lock rotation of marker when drawing
                    transform.rotation = lastTouchRot;

                    //apply new changes to the whiteboard
                    whiteboard.texture.Apply();
                }

                //update drawing rotation and position
                lastTouchPos = new Vector2(x, y);
                lastTouchRot = transform.rotation;
                touchedLastFrame = true;
                return;
            }
        }

        whiteboard = null;
        touchedLastFrame = false;
    }
}
