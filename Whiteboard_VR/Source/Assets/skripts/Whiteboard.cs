using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Whiteboard : MonoBehaviour
{
    public Texture2D texture;
    public Vector2 textureSize = new Vector2(x:2048, y:2048);
    private Renderer r;
    private Color color;
    private Color[] pixels;

    void Start()
    {
        r = GetComponent<Renderer>();
        texture = new Texture2D((int) textureSize.x, (int) textureSize.y);

        //color in whiteboard
        color = r.material.color;
        pixels = Enumerable.Repeat(color, texture.width * texture.height).ToArray();
        texture.SetPixels(pixels);
        texture.Apply();

        r.material.mainTexture = texture;
    }
}
