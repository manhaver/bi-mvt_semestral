# VR interactable whiteboard
Basic whiteboard with ability to draw and erase just like in real world. 

## Used tools and packages
Project was made with Unity and with XR Interaction toolkit plus OpenXR plugin.

In the absence of headset is usable as desktop version using XR Device Simulator.

For the player movement using XR Locomotion Systen (Action-based) and for hand and camera movement using XR Origin (Action-based).

## Skripts
Whiteboard project uses 3 c# scripts. 

Whiteboard script is for initializing texture and painting it its color. 

Marker script is made to find the pixel, where we touched the whiteboard texture and fill that texture accordingly to the size of marker. (Close description in code)

Eraser script fills whiteboard texture with the base color. This script is paired to the button on eraser and is executed when the player presses it.

## Desktop controls
In the desktop version I used default settings from XR Device Simulator. 

Left Hand is controled using left shift key, right hand using space bar.

Camera movement using left shift and WSAD, rotation QE.

Grab using G key and trigger action using left mouse key (while pressing left shift for the left hand or space bar for the right hand).

Right hand can move forwards and backwards using scroll wheel (while pressing the space bar).

## Video
In video using desktop version.

![](video/MVT_VR.mp4)
